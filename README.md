# ohos_cross_tools

# 介绍
针对OpenHarmony的linux版本提供工具移植，当前支持工具列表如下：

| name       | 本文档支持 | 脚本 |
| ---------- | ---------- | ---- |
| gdb        | Y          | Y    |
| strace     | Y          | Y    |
| bash       | Y          | Y    |
| openssl    | Y          | Y    |
| python     | Y          | Y    |
| vim        | Y          | Y    |
| curl       | Y          | Y    |
| elfutils   | Y          |      |
| util-linux | Y          | Y    |
| coreutils  | Y          | Y    |
| valgrind   | Y          | Y    |
| htop       | Y          |      |
| jemalloc   | Y          | Y    |

脚本在scripts目录中，默认通过docker编译，定制run_docker.sh中的BUILD_LIST去修改打包工具集合。BUILD_ARCH可以改为arm或者aarch64.



arm/arm64的编译产物临时归档到（2022.05.01）：

链接:https://pan.baidu.com/s/15fBMqhamfj3AbGGzSJ9Oww 
提取码:2imm



# 工具链准备

基于 [musl-cross-make](https://github.com/richfelker/musl-cross-make)，该项目是为linux musl创建的，为了适配ohos需要做一些小修改，我fork了一个仓

https://gitee.com/stesen/musl-cross-make_for_openharmony

里面修改了不少，可以看下git log

## 准备编译工具链

```
git clone https://gitee.com/stesen/musl-cross-make_for_openharmony
pushd  musl-cross-make_for_openharmony
```

## 创建config.mak

如果是arm：

```shell
export TOOLCHAIN_TARGET=arm-linux-musleabi
```

如果是arm64

```shell
export TOOLCHAIN_TARGET=aarch64-linux-musleabi
```

然后创建config：

```shell
cat > config.mak << EOF
OHOS_DIR = XXXX/openharmony
TARGET = ${TOOLCHAIN_TARGET}
OUTPUT = /opt/cross

LINUX_FROM = ohos
LINUX_VER = 5.10
MUSL_FROM = ohos
MUSL_VER = ohos
GCC_VER = 10.1.0
EOF
```

## 编译安装工具链

```shell
sudo install -m 777 -d /opt/cross
make && make install
popd

## musl 1.2.2需要额外定义个宏：
sed -i '15i#define FNM_EXTMATCH 0' /opt/cross/${TOOLCHAIN_TARGET}/include/fnmatch.h

## 如果要提供给buildroot编译，需要建个链接
mkdir /opt/cross/arm-linux-musleabi/usr
ln -svf ../include /opt/cross/arm-linux-musleabi/usr/
```

OHOS_DIR是指openharmony代码路径，我们需要用到其中的third_party/musl，因为ohos的musl做了很多修改，官方release的版本跑起来会有很多问题。或者也可以从gitee上直接拉musl这个仓，我懒得改git的下载方式了，大家自便。

注意上面构建的目标是arm eabi的，ohos的linker生成在这个位置 /opt/cross/arm-linux-musleabi/system/lib ，比较下和自己单板上跑的linker是否名字一致。

编译过程应该需要host安装一些autotools，make，gcc之类的基本工具，我环境是manjaro x86_64。其他环境没有试过，但应该没有啥特别的，缺啥装啥。

# 准备运行目录和变量

```shell
export PATH=/opt/cross/bin:${PATH}
export DEBUGROOT=/data/debugroot
export TOOLCHAIN_PREFIX=${TOOLCHAIN_TARGET}-
export CC=${TOOLCHAIN_PREFIX}gcc
export STRIP=${TOOLCHAIN_PREFIX}strip
export LD_LIBRARY_PATH=${DEBUGROOT}/lib
export LDFLAGS=-L${DEBUGROOT}/lib
export PKG_CONFIG_PATH=${DEBUGROOT}/pkgconfig
export CONFIGURE_ARGS="--host=${TOOLCHAIN_TARGET} --prefix=$DEBUGROOT"

sudo mkdir -pv $DEBUGROOT/lib
```

把工具链的库拷到运行时路径下

```shell
cp -rav /opt/cross/${TOOLCHAIN_TARGET}/lib/* ${DEBUGROOT}/lib
```

# 编译strace

```shell
wget https://strace.io/files/5.14/strace-5.14.tar.xz
tar xf strace-5.14.tar.xz
pushd strace-5.14
./configure $CONFIGURE_ARGS &&
    make &&
    make install
popd
```

编译openssl
===

这里编译openssl是为了下面python，虽然不是强依赖，有总比没有好

```shell
wget https://github.com/openssl/openssl/archive/refs/tags/OpenSSL_1_1_1l.tar.gz
tar xf OpenSSL_1_1_1l.tar.gz
pushd openssl-OpenSSL_1_1_1l
./Configure linux-armv4 --prefix=$DEBUGROOT \
    --openssldir=${DEBUGROOT}/etc/tls \
    shared no-ssl no-srp no-tests &&
    make && make install
popd
```

编译python
===

这里做的python工具主要是为了下面gdb用的，本身没有带多少模块和插件，也没有pip，后面再说吧，反正ohos有python-sig去负责这个事

```shell
wget https://www.python.org/ftp/python/3.9.7/Python-3.9.7.tgz
tar xf Python-3.9.7.tgz
pushd Python-3.9.7
./configure $CONFIGURE_ARGS --build=arm \
    --enable-shared=yes --without-ensurepip \
    ac_cv_little_endian_double=yes \
    ac_cv_file__dev_ptmx=yes \
    ac_cv_file__dev_ptc=no \
    ac_cv_func_wcsftime=no \
    ac_cv_func_ftime=no \
    ac_cv_func_faccessat=no \
    ac_cv_posix_semaphores_enabled=no \
    ac_cv_buggy_getaddrinfo=no \
    ac_cv_func_sem_open=no \
    ac_cv_func_sem_timedwait=no \
    ac_cv_func_sem_getvalue=no \
    ac_cv_func_sem_unlink=no \
    ac_cv_file__dev_ptmx=yes \
    ac_cv_file__dev_ptc=no \
    ac_cv_func_wcsftime=no &&
    make && make install
popd
```

# 编译gdb

## 解决libgmp依赖

gdb依赖libgmp

```shell
wget https://gmplib.org/download/gmp/gmp-6.2.1.tar.lz
tar xf gmp-6.2.1.tar.lz
pushd gmp-6.2.1
./configure $CONFIGURE_ARGS &&
    make && make install
popd
```

解决libmpfr依赖
---

gdb可以不依赖mpfr，不过我看很多工具链会编译它，我也不知道为啥，反正就编译下，也许能让gdb过得舒服点

```shell
wget https://www.mpfr.org/mpfr-current/mpfr-4.1.0.tar.gz
tar xf mpfr-4.1.0.tar.gz
pushd mpfr-4.1.0
./configure $CONFIGURE_ARGS \
    --with-gmp-include=$DEBUGROOT/include \
    --with-gmp-lib=$DEBUGROOT/lib &&
    make & make install
popd
```

解决python依赖
---

gdb可以依赖python，这样就可以搞一些骚脚本，如果不需要，那跳过本步骤，并且下面gdb的configure参数去掉--with-python参数。

先把上面的python编译好，确保在单板上运行python3能跑起来，然后我们需要做个假的python-config去骗下交叉编译时gdb的configure：

```shell
cat << EOF | sudo tee /data/my-python-config
#!/bin/sh

case $2 in
    --includes)
        echo "-I$DEBUGROOT/include/python3.9"
        ;;
    --ldflags)
        echo "-L$DEBUGROOT/lib -lpython3.9 -export-dynamic"
        ;;
    --exec-prefix)
        echo "$DEBUGROOT"
        ;;
    *)
        echo bad args $2; exit 1
esac

exit 0
EOF
sudo chmod +x /data/my-python-config
```

## gdb

```shell
wget https://ftp.gnu.org/gnu/gdb/gdb-11.1.tar.gz
tar xf gdb-11.1.tar.gz
pushd gdb-11.1
./configure $CONFIGURE_ARGS \
    --disable-source-highlight \
    --with-mpfr=$DEBUGROOT \
    --with-gmp=$DEBUGROOT \
    --with-python=/data/my-python-config &&
    make && make install
popd
```

编译bash
===

```shell
wget https://ftp.gnu.org/gnu/bash/bash-5.1.tar.gz
tar xf bash-5.1.tar.gz
pushd bash-5.1
./configure $CONFIGURE_ARGS \
    --without-bash-malloc &&
    make && make install
popd
```

vim
===

解决ncurses依赖
---

vim依赖tlib，建议用ncurses

```shell
wget https://ftp.gnu.org/gnu/ncurses/ncurses-6.3.tar.gz
tar xf ncurses-6.3.tar.gz
pushd ncurses-6.3
./configure $CONFIGURE_ARGS \
    --disable-stripping \
    --enable-const \
    --enable-ext-colors \
    --enable-ext-mouse \
    --enable-overwrite \
    --enable-pc-files \
    --enable-termcap \
    --enable-widec \
    --without-ada \
    --without-cxx-binding \
    --without-tests \
    --without-debug \
    --with-normal \
    --with-static \
    --with-shared \
    ac_cv_header_locale_h=no &&
    make && make install
popd
ln -svf libncursesw.so ${DEBUGROOT}/lib/libncurses.so
```

vim
---

```shell
wget ftp://ftp.vim.org/pub/vim/unix/vim-8.1.tar.bz2
tar xf vim-8.1.tar.bz2
pushd vim81

./configure $CONFIGURE_ARGS \
    --enable-gui=no \
    vim_cv_toupper_broken=yes \
    vim_cv_getcwd_broken=no \
    vim_cv_tgetent=zero \
    vim_cv_terminfo=yes \
    vim_cv_stat_ignores_slash=no \
    vim_cv_memmove_handles_overlap=yes \
    vim_cv_tty_group=world \
    --with-local-dir=${DEBUGROOT} \
    --with-tlib=ncurses &&\
    make && make install
popd
```

curl
===

```shell
wget https://curl.se/download/curl-7.80.0.tar.xz
tar xf curl-7.80.0.tar.xz
pushd curl-7.80.0
autoreconf -vfi &&
    ./configure $CONFIGURE_ARGS \
    --with-openssl \
    --without-libidn \
    --without-libidn2 \
    --disable-ldap \
    --with-pic \
    --without-libssh2 &&
    make && make install
popd
```


---

elfutils
===

解决zlib依赖
---

```shell
wget https://www.zlib.net/fossils/zlib-1.2.11.tar.gz
tar xf zlib-1.2.11.tar.gz
pushd zlib-1.2.11
./configure --prefix=$DEBUGROOT && make && make install
popd
```

解决xz依赖
---

```shell
wget https://tukaani.org/xz/xz-5.2.5.tar.gz
tar xf xz-5.2.5.tar.gz
pushd xz-5.2.5
./configure $CONFIGURE_ARGS &&
    make && make install
popd
```

解决bzip2依赖
---

```shell
wget https://sourceware.org/pub/bzip2/bzip2-1.0.8.tar.gz
tar xf bzip2-1.0.8.tar.gz
pushd bzip2
sed -i "s/CC=gcc/CC=${TOOLCHAIN_PREFIX}gcc/g" Makefile-libbz2_so &&
    make &&
    install -D -m644 bzlib.h ${DEBUGROOT}/include/bzlib.h \
    install -D -m755 libbz2.so.1.0.8 ${DEBUGROOT}/lib/libbz2.so.1.0 &&
    ln -svf libbz2.so.1.0 /data/debugroot/lib/libbz2.so
popd
```

解决zstd依赖
---

```shell
wget https://github.com/facebook/zstd/archive/v1.5.0.tar.gz -O zstd-v1.5.0.tar.gz
tar xf zstd-v1.5.0.tar.gz
pushd zstd-1.5.0
export CC=${TOOLCHAIN_PREFIX}gcc
export CXX=${TOOLCHAIN_PREFIX}g++
make -C lib HAVE_PTHREAD=1 HAVE_ZLIB=0 HAVE_LZMA=0 HAVE_LZ4=0 lib-mt &&
    make -C programs HAVE_PTHREAD=1 HAVE_ZLIB=0 HAVE_LZMA=0 HAVE_LZ4=0 &&
    make -C contrib/pzstd &&
    make PREFIX="" DESTDIR=${DEBUGROOT} install
unset CC
unset CXX
popd
```

解决argp依赖
---

argp是glibc提供的特性，elfutils依赖它，musl自己不带argp，需要使用argp-standalone来补充。

```shell
wget http://www.lysator.liu.se/~nisse/misc/argp-standalone-1.3.tar.gz
tar xf argp-standalone-1.3.tar.gz
pushd argp-standalone-1.3
CFLAGS="-fPIC -U__OPTIMIZE__" \
    ./configure $CONFIGURE_ARGS &&
    make && make install &&
    install -D -m644 argp.h ${DEBUGROOT}/include/argp.h &&
    install -D -m755 libargp.a ${DEBUGROOT}/lib/libargp.a
popd
```

解决fts依赖
---

```shell
git clone https://github.com/pullmoll/musl-fts.git
pushd musl-fts
./bootstrap.sh &&
    CFLAGS=-fPIC ./configure $CONFIGURE_ARGS &&
    make && make install
popd
```

解决obstack依赖
---

```shell
git clone https://github.com/void-linux/musl-obstack.git
pushd musl-obstack
./bootstrap.sh &&
    ./configure $CONFIGURE_ARGS &&
    make && make install
popd
```

elfutils
---

```shell
wget https://sourceware.org/elfutils/ftp/0.186/elfutils-0.186.tar.bz2
tar xf elfutils-0.186.tar.bz2
pushd elfutils-0.186
CFLAGS=-I/data/debugroot/include \
    LIBS="-lz -lzstd -lbz2 -llzma -lfts -l obstack" \
    ./configure $CONFIGURE_ARGS \
    --disable-debuginfod \
    --disable-libdebuginfod \
    --disable-werror &&
    make && make install
popd
```

# util-linux

```shell
wget https://mirrors.edge.kernel.org/pub/linux/utils/util-linux/v2.37/util-linux-2.37.2.tar.gz
tar xf util-linux-2.37.2.tar.gz
pushd util-linux-2.37.2
CFLAGS=-I${DEBUGROOT}/include \
    ./configure $CONFIGURE_ARGS \
    --sbindir=${DEBUGROOT}/bin \
    --without-systemd \
    --disable-makeinstall-chown \
    --disable-bash-completion &&
    make && make install
popd
```

# coreutils

```shell
wget https://ftp.gnu.org/gnu/coreutils/coreutils-9.0.tar.xz
tar xf coreutils-9.0.tar.xz 
pushd coreutils-9.0/
./configure $CONFIGURE_ARGS && make && make install
popd
```

# valgrind

```shell
wget https://sourceware.org/pub/valgrind/valgrind-3.18.1.tar.bz2
tar xf valgrind-3.18.1.tar.bz2 
pushd valgrind-3.18.1
sed -i "s/armv7/arm/g" configure
./configure $CONFIGURE_ARGS && make && make install
popd
```

# htop

首先编译上面的ncurses，然后编译libnl

## 解决libnl依赖

```shell
wget https://www.infradead.org/\~tgr/libnl/files/libnl-3.2.25.tar.gz
tar xf libnl-3.2.25.tar.gz
pushd libnl-3.2.25
./configure $CONFIGURE_ARGS && make && make install
popd
```

## 编译htop

```shell
wget https://github.com/htop-dev/htop/releases/download/3.2.0/htop-3.2.0.tar.xz
tar xf htop-3.2.0.tar.xz
pushd htop-3.2.0
CFLAGS=-I${DEBUGROOT}/include ./configure $CONFIGURE_ARGS --disable-unicode --includedir=${DEBUGROOT}/include && make && make install
popd
```

注意htop运行需要保留terminfo信息，我选择的TERM是linux，需要/data/debugroot/share/terminfo/l/linux

# 编译jemalloc

OHOS是用musl分配器的，这是个垃圾。后面会切换mimalloc，但是有些jemalloc的调试功能很怀念，因此移植一个来用。

```shell
wget https://github.com/jemalloc/jemalloc/releases/download/5.3.0/jemalloc-5.3.0.tar.bz2
tar xf jemalloc-5.3.0.tar.bz2
pushd jemalloc-5.3.0
./configure $CONFIGURE_ARGS --enable-prof --enable-stats --enable-debug && make && make install
popd
```



# 设备侧运行

先建个脚本

```shell
cat > ${DEBUGROOT}/init.sh << EOF
export PATH=/data/debugroot/bin:\$PATH
export LD_LIBRARY_PATH=/data/debugroot/lib:\$LD_LIBRARY_PATH
bash
EOF
chmod +x ${DEBUGROOT}/init.sh
```

通过hdc或者nfs推送到设备/data/debugroot目录下（可以提前删掉里面的manpages、头文件、静态库等，减少体积）

运行/data/debugroot/init.sh，就可以运行其中的工具了

# 使用说明

1.  你们自己用用就算了，有问题我也解决不了，如果你知道怎么解决，告诉我，我请你喝咖啡

# 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
