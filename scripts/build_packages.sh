#!/bin/bash

MUST_LIST="bash"
DONE_DIR=${BUILD_DIR}/.done

do_build_depends() {
	depends_file=${SCRIPTS_DIR}/packages/${1}/depends
	[ -f $depends_file ] || return
	while read pkg; do
		[ "X$pkg" != "X" ] &&	do_build_package $pkg
	done < $depends_file
}

checkdone() {
	return [ -f $1 ]
}

setdone() {
	[ -d $DONE_DIR ] || mkdir ${DONE_DIR}
	touch $1
}

package_script_wrapper() {
	done_file=${DONE_DIR}/$1
	if [ -f $done_file ]; then
		echo ">>> $1 has already been built, skipping...."
		echo "Remove $done_file if you want to build again"
		return
	fi
	package_script=${SCRIPTS_DIR}/packages/${1}/build.sh
	[ -f $package_script ] && \
		echo "  ==== Start building $1 ====" && \
		bash -ex $package_script 2>&1 | tee ${BUILD_DIR}/${1}.log && setdone $done_file
}

do_build_package() {
	if [ -d ${SCRIPTS_DIR}/packages/$1 ] ; then
		do_build_depends $1
		package_script_wrapper $1
	else
		echo "*** $1 build scripts not exists"
	fi
}

clean_up_last_build() {
    rm -fr ${BUILD_DIR}/* ${BUILD_DIR}/.done ${DEBUGROOT}
}

copy_toolchain_libs() {
	[ -d $DEBUGROOT/lib ] || mkdir -pv $DEBUGROOT/lib
	cp -ra /opt/tools/${TOOLCHAIN_TARGET}/lib/* ${DEBUGROOT}/lib
	mkdir $DEBUGROOT/include
}

create_init() {
	echo "Create init.sh ..."
	cat > ${DEBUGROOT}/init.sh << EOF
export HOME=${DEBUGROOT}/home
export PATH=${DEBUGROOT}/bin:\$PATH
export LD_LIBRARY_PATH=${DEBUGROOT}/lib:\$LD_LIBRARY_PATH
bash
EOF
	chmod +x ${DEBUGROOT}/init.sh

	[ -d ${DEBUGROOT}/home ] || mkdir ${DEBUGROOT}/home
	cat > ${DEBUGROOT}/home/.bashrc << EOF
export PS1='[\u@ohos \w]\$ '
alias ll='ls -l'
alias ps='ps -Af'

function resize_term() {
  old=\$(stty -g)
  stty raw -echo min 0 time 5

  printf '\033[18t' > /dev/tty
  IFS=';t' read -r _ rows cols _ < /dev/tty
  stty "\$old"
  export COLUMNS=\$cols
  export LINES=\$rows
}
alias vim='resize_term; TERM=xterm vim'
EOF
	cat > ${DEBUGROOT}/home/.inputrc << EOF
"\e[3~": delete-char
"\e[1~": beginning-of-line
"\e[4~": end-of-line
EOF
}

do_cleanup() {
	echo "clean up ..."
	rm -fr ${DEBUGROOT}/include
	rm -fr ${DEBUGROOT}/{doc,info,man,tabset,local}
	rm -fr ${DEBUGROOT}/lib/python3.9/distutils/tests ${DEBUGROOT}/lib/python3.9/unittest ${DEBUGROOT}/lib/python3.9/test
	find ${DEBUGROOT}/bin ${DEBUGROOT}/lib -type f \( -name "*.a" -or -name "*.po" -or -name "*.la" \) -exec rm {} \;
	find ${DEBUGROOT}/bin ${DEBUGROOT}/lib -type f -exec ${TOOLCHAIN_TARGET}-strip {} \;
}

pre_cleanup() {
	rm -fr ${DEBUGROOT}_backup
	cp -ra ${DEBUGROOT} ${DEBUGROOT}_backup
}

post_cleanup() {
	rm -fr ${DEBUGROOT}
	mv ${DEBUGROOT}_backup ${DEBUGROOT}
}

do_tgz() {
	echo "packaging ..."
	cd $(dirname $DEBUGROOT)
	tar cfz debugroot_${BUILD_ARCH}.tgz debugroot

	ls -lsh debugroot_${BUILD_ARCH}.tgz
}

if [ "X$BUILD_DIR" == "X" ] ; then
	echo export BUILD_DIR first
	exit 1
fi

cd $BUILD_DIR

clean_up_last_build
copy_toolchain_libs

export CONFIGURE_ARGS="--host=${TOOLCHAIN_TARGET} --prefix=${DEBUGROOT}"

for pkg in $MUST_LIST; do
	do_build_package $pkg
done
for pkg in $BUILD_LIST; do
	do_build_package $pkg
done

${BUILD_RELEASE} && create_init && pre_cleanup && do_cleanup && post_cleanup && do_tgz
