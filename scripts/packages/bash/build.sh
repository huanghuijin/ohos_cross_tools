wget https://ftp.gnu.org/gnu/bash/bash-5.1.tar.gz
tar xf bash-5.1.tar.gz
pushd bash-5.1
./configure $CONFIGURE_ARGS \
    --without-bash-malloc &&
    make && make install
popd
