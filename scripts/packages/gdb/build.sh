wget https://ftp.gnu.org/gnu/gdb/gdb-11.1.tar.gz
tar xf gdb-11.1.tar.gz
pushd gdb-11.1
./configure $CONFIGURE_ARGS \
    --disable-source-highlight \
    --with-mpfr=$DEBUGROOT \
    --with-gmp=$DEBUGROOT \
    --with-python=${BUILD_DIR}/python-config &&
    make && make install
popd
