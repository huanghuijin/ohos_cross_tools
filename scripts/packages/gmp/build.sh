wget https://gmplib.org/download/gmp/gmp-6.2.1.tar.xz
tar xf gmp-6.2.1.tar.xz
pushd gmp-6.2.1
./configure $CONFIGURE_ARGS &&
    make && make install
popd
