wget https://www.mpfr.org/mpfr-current/mpfr-4.1.0.tar.gz
tar xf mpfr-4.1.0.tar.gz
pushd mpfr-4.1.0
./configure $CONFIGURE_ARGS \
    --with-gmp-include=$DEBUGROOT/include \
    --with-gmp-lib=$DEBUGROOT/lib &&
    make & make install
popd
