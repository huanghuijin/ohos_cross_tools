wget https://ftp.gnu.org/gnu/ncurses/ncurses-6.3.tar.gz
tar xf ncurses-6.3.tar.gz
pushd ncurses-6.3
./configure $CONFIGURE_ARGS \
    --disable-stripping \
    --enable-const \
    --enable-ext-colors \
    --enable-ext-mouse \
    --enable-overwrite \
    --enable-pc-files \
    --enable-termcap \
    --enable-database \
    --enable-widec \
    --without-ada \
    --without-cxx-binding \
    --without-tests \
    --without-debug \
    --with-normal \
    --with-static \
    --with-shared \
    --disable-big-core \
    --without-fallbacks \
    ac_cv_header_locale_h=no &&
    make && make install
popd
ln -svf libncursesw.so ${DEBUGROOT}/lib/libncurses.so
