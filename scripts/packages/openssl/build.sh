wget https://www.openssl.org/source/openssl-1.1.1l.tar.gz
tar xf openssl-1.1.1l.tar.gz
pushd openssl-1.1.1l

case $BUILD_ARCH in
    arm)
        CFGTARGET=linux-armv4
        ;;
    aarch64)
        CFGTARGET=linux-aarch64
        ;;
    *)
        ;;
esac

./Configure ${CFGTARGET} --prefix=$DEBUGROOT \
    --openssldir=${DEBUGROOT}/etc/tls \
    shared no-ssl no-srp no-tests &&
    make && make install

popd
