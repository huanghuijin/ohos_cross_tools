PYTHON_VERSION=$(python --version | awk '{print $2}')
PYTHON_VERSION_SHORT=$(echo ${PYTHON_VERSION} | grep -oE "^[0-9]+\.[0-9]+")
PYTHON_CONFIG_FILE="$(dirname ${0})/python-config"

wget https://www.python.org/ftp/python/${PYTHON_VERSION}/Python-${PYTHON_VERSION}.tgz
tar xf Python-${PYTHON_VERSION}.tgz
pushd Python-${PYTHON_VERSION}
./configure $CONFIGURE_ARGS --build=$BUILD_ARCH \
    --enable-shared=yes --without-ensurepip --disable-ipv6 \
    ac_cv_little_endian_double=yes \
    ac_cv_file__dev_ptmx=yes \
    ac_cv_file__dev_ptc=no \
    ac_cv_func_wcsftime=no \
    ac_cv_func_ftime=no \
    ac_cv_func_faccessat=no \
    ac_cv_posix_semaphores_enabled=no \
    ac_cv_buggy_getaddrinfo=no \
    ac_cv_func_sem_open=no \
    ac_cv_func_sem_timedwait=no \
    ac_cv_func_sem_getvalue=no \
    ac_cv_func_sem_unlink=no \
    ac_cv_file__dev_ptmx=yes \
    ac_cv_file__dev_ptc=no \
    ac_cv_func_wcsftime=no &&
    make && make EXTRATESTOPTS=--list-tests install &&
    sed "s/python3.9/python${PYTHON_VERSION_SHORT}/g" ${PYTHON_CONFIG_FILE} > ${BUILD_DIR}/python-config &&
    chmod +x ${BUILD_DIR}/python-config
popd
