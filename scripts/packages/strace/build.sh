#!/bin/bash

wget https://strace.io/files/5.14/strace-5.14.tar.xz
tar xf strace-5.14.tar.xz
pushd strace-5.14
./configure $CONFIGURE_ARGS --enable-mpers=no &&
    make &&
    make install
popd
