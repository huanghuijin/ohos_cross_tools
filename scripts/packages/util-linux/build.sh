wget https://mirrors.edge.kernel.org/pub/linux/utils/util-linux/v2.37/util-linux-2.37.2.tar.gz
tar xf util-linux-2.37.2.tar.gz
pushd util-linux-2.37.2
CFLAGS=-I${DEBUGROOT}/include \
    ./configure $CONFIGURE_ARGS \
    --sbindir=${DEBUGROOT}/bin \
    --without-systemd \
    --disable-makeinstall-chown \
    --disable-bash-completion &&
    make && make install
popd
