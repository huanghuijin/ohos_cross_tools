wget https://sourceware.org/pub/valgrind/valgrind-3.18.1.tar.bz2
tar xf valgrind-3.18.1.tar.bz2
pushd valgrind-3.18.1
case $BUILD_ARCH in
    arm)
        sed -i "s/armv7/arm/g" configure
        ;;
    *)
        ;;
esac
./configure $CONFIGURE_ARGS && make && make install
popd
