wget http://ftp.vim.org/pub/pub/vim/unix/vim-8.2.tar.bz2
tar xf vim-8.2.tar.bz2
pushd vim82
./configure $CONFIGURE_ARGS \
    --enable-gui=no \
    vim_cv_toupper_broken=yes \
    vim_cv_getcwd_broken=no \
    vim_cv_tgetent=zero \
    vim_cv_terminfo=yes \
    vim_cv_stat_ignores_slash=no \
    vim_cv_memmove_handles_overlap=yes \
    vim_cv_tty_group=world \
    --with-local-dir=${DEBUGROOT} \
    --with-tlib=ncurses &&\
    make && make install
popd
